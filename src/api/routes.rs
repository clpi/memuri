use actix_web::*;
use crate::api::AppState;
use crate::db::models::User;

#[get("/")]
async fn index(data: web::Data<AppState>) 
        -> Result<HttpResponse, Error> {

    let mut string = String::from("Hello there!\n");
    string.push_str("GET /users: List users\n");
    string.push_str("GET /{username}/entries: List user's entries\n");
    string.push_str("GET /{username}/tasks: List user's tasks\n");
    string.push_str( "GET /{username}/rules: List user's rules\n" );
    string.push_str( "GET /{username}/logs: List user's rule logs/habits\n");
    string.push_str( "GET /{username}/{date}: List user's content for date\n" );
    string.push_str( "POST /{username}/new_entry/{content}: Make new entry\n" );

    Ok(HttpResponse::Ok().body(string))

    
}

#[get("/users")]
async fn get_users(data: web::Data<AppState>) -> impl Responder {
    let query: Vec<User> = data.db.get_users().await.unwrap();
    let users: String = query.into_iter()
        .map(| user: User | -> String { user.username })
        .collect();

    HttpResponse::Ok().body(users)
}

#[get("/add_test_users")]
async fn add_test_users(data: web::Data<AppState>) -> impl Responder {
    test(data.db.clone()).await;
    HttpResponse::Ok().body("test users added")
}


#[get("/about")]
async fn about(data: web::Data<AppState>) -> impl Responder {
    let app_name = &data.name;
    HttpResponse::Ok().body(
        format!("About {}", app_name)
    )
}

pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.service(index);
    cfg.service(about);
    cfg.service(get_users);
    cfg.service(add_test_users);
}

async fn test(db: crate::db::database::Database) {
    db.add_user(
        String::from("tst@example.com"),
        String::from("test_user"),
        String::from("test_pas")
    );
    db.add_user(
        String::from("tst23r2@example.com"),
        String::from("sir_user"),
        String::from("sir_pass")
    );
}

