use actix_web::{dev::Payload, error::BlockingError, web, Error, FromRequest, HttpRequest, HttpResponse, ResponseError};
use actix_identity::Identity;
use serde::Deserialize;
use futures::future::{Ready, ok, err};
use crate::db::models::User;


#[derive(Debug, Deserialize)]
pub struct AuthData {
    username: String,
    password: String,
}


pub type LoggedUser = User;

impl FromRequest for LoggedUser {
    type Config = ();
    type Error = Error;
    type Future = Ready<Result<LoggedUser, Error>>;

    fn from_request(req: &HttpRequest, pl: &mut Payload) -> Self::Future{
        let req_id = Identity::from_request(req, pl).into_inner();
        if let Ok(identity) = req_id {
            if let Some(user_json) = identity.identity() {
                if let Ok(user) = serde_json::from_str(&user_json) {
                    return ok(user);
                }
            }
        }
        err(HttpResponse::Unauthorized().json("Unauthorized").into())
    }


}

pub async fn register(user_data: web::Json<AuthData>) {


}
