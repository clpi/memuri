use crate::db::database::Database;
use actix_web_httpauth::extractors::{bearer::{BearerAuth, Config}, AuthenticationError};
use actix_web_httpauth::middleware::HttpAuthentication;
use actix_web::*;
use actix_web::dev::ServiceRequest;
use argonautica::{Hasher, Verifier};

pub mod routes;
mod models;
mod auth;

// pub use routes::init;
pub use models::*;
pub use auth::*;
pub use routes::*;

pub struct AppState {
    pub name: String,
    pub db: Database,
}

pub async fn validate(req: ServiceRequest, cred: BearerAuth) -> Result<ServiceRequest, Error> {
    let config = req
        .app_data::<Config>()
        .map(|data| data.get_ref().clone())
        .unwrap_or_else(Default::default);
    Ok(req)
}

pub fn gen_hashed_pw(pw: String) -> Result<String, argonautica::Error> {
    Hasher::new().with_password(pw).hash()
}
