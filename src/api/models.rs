/*
use serde::{Serialize, Deserialize};
use actix_web::{HttpResponse, HttpRequest, Responder, Error};
use futures::future::{ready, Ready};
use sqlx::{SqlitePool, FromRow, Row};
use sqlx::sqlite::SqliteRow;

#[derive(Serialize, Deserialize)]
pub struct UserReq {

}

#[derive(Serialize, FromRow)]
pub struct User {
    pub uid: i32,
    pub email: String,
    pub username: String,
    pub password: String, //this is temporary obviously
    pub date_created: i32,
}

impl Responder for User {
    type Error = Error;
    type Future = Ready<Result<HttpResponse, Error>>;

    fn respond_to(self, _req: &HttpRequest) -> Self::Future {
        let body = serde_json::to_string(&self).unwrap();
        ready(Ok(
            HttpResponse::Ok()
                .content_type("application/json")
                .body(body)
        ))
    }
}

impl User {
    pub async fn get_all(db: &sqlx::SqlitePool) -> sqlx::Result<Vec<User>> {
        let users: Vec<User> = vec![];
        let user_query = sqlx::query(r#"SELECT * FROM Users;"#)
            .execute(db)
            .await?;
        Ok(vec![
            User {
                uid:0, 
                email:String::from(""), 
                username:String::from(""), 
                password:String::from(""),
                date_created:0
            }     
        ])
    }

    pub async fn get_by_id(uid: i32, db: &sqlx::SqlitePool) -> sqlx::Result<User> {
        Ok(User {
            uid:0, 
            email:String::from(""), 
            username:String::from(""), 
            password:String::from(""),
            date_created:0
        }) 
    }

    pub async fn create(user: UserReq, db: &sqlx::SqlitePool) -> sqlx::Result<User> {
        Ok(User {
            uid:0, 
            email:String::from(""), 
            username:String::from(""), 
            password:String::from(""),
            date_created:0
        }) 
    }

    pub async fn update(uid: i32, user: UserReq, db: &sqlx::SqlitePool) -> sqlx::Result<User> {
        Ok(User {
            uid:0, 
            email:String::from(""), 
            username:String::from(""), 
            password:String::from(""),
            date_created:0
        }) 
    }

    pub async fn delete(uid: i32, db: &sqlx::SqlitePool) -> sqlx::Result<u64> {
        Ok(3 as u64)        
    }
}
*/
