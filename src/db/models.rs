extern crate sqlx;

use sqlx::types::chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, sqlx::FromRow)]
pub struct User {
    pub uid: i32,
    pub email: String,
    pub username: String,
    pub password: String,
    pub date_created: i32,
}

pub struct UserDB {
    pub uid: Option<i32>,
    pub email: String,
    pub username: String,
    pub password: String,
    pub date_created: i32,
}


pub struct Entry {
    pub eid: Option<i32>,
    pub uid: Option<i32>,
    pub status: i32,
    pub label: Option<String>,
    pub content: String,
    pub date_created: i32,
}

pub struct BgRecord {
    pub bid: Option<i32>,
    pub uid: i32,
    pub date_created: i32,
}

pub struct FoodRecord {
    pub feid: Option<i32>,
    pub uid: Option<i32>,
    pub fname: String,
    pub cal: i32,
    pub prot: i32,
    pub fat: i32, 
    pub carb: i32,
    pub date_created: i32,
    pub label: Option<String>,
}

pub struct GenericRecord {
    pub gid: Option<i32>,
    pub uid: Option<i32>,
    pub record_key: String,
    pub record_value: String,
    pub label: Option<i32>,
}

pub struct Task {
    pub tid: Option<i32>,
    pub uid: Option<i32>,
    pub task: String,
    pub label: Option<String>,
    pub status: i32,
    pub priority: i32,
    pub date_due: i32,
    pub date_created: i32,
}

#[derive(sqlx::FromRow)]
pub struct Rule {
    pub rid: i32,
    pub uid: i32,
    pub rule: String,
    pub label: String,
    pub priority: i32,
    pub date_created: i32,
}

pub struct RuleLog {
    pub lid: Option<i32>,
    pub rid: Option<i32>,
    pub status: i32,
    pub date_created: i32,
}


impl User { 

    fn new(uname: &str, entries: Option<Vec<Entry>>) -> Self {
        let date: DateTime<Utc>  = Utc::now();
        Self { 
            uid: 0,
            email: String::from(""),
            username: uname.to_string(),
            password: String::from(""),
            date_created: Utc::now().timestamp() as i32,
        }
    }

    fn greet(&self) { 
        println!("Hello, I'm {} (id: {}, created {})", 
            self.username, 
            self.uid.to_string(),
            self.date_created.to_string());
    }


    //fn add_entry(&self, content: &str) {
        //self.entries.push(Entry::new(self.uid.unwrap(), content));
    //}

}

impl Entry {

    fn new(uid: i32, content: &str) -> Self {
        Self {
            eid: Some(0),
            uid: Some(0),
            date_created: Utc::now().timestamp() as i32,
            status: 0,
            content: content.to_string(),
            label: Some(String::from("")),
        }
    }
}

