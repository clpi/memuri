// use sqlx::{SqlitePool, SqliteConnection};
use sqlx::sqlite::{SqliteConnection, SqliteRow, SqliteError, SqliteValue, SqlitePool};
use sqlx::Connect;
use sqlx::prelude::*;
use sqlx::types::chrono::{DateTime, Utc};
use sqlx::pool::PoolConnection;
use argonautica::{Hasher, Verifier};
use crate::db::models::{User, Entry, BgRecord, GenericRecord, Rule, RuleLog, FoodRecord, Task};

#[derive(Clone)]
pub struct Database {
    pub pool: SqlitePool,
    pub tables: Vec<String>,
}



impl Database {

    pub async fn new(url: &str) -> sqlx::Result<Self> {
        let tables = vec![
            String::from("Users"), 
            String::from("Tasks"),
            String::from("Entries"),
            String::from("BgEntries"),
            String::from("FoodRecords"),
            String::from("GenericRecords"),
            String::from("Rules"),
            String::from("RulesLog")
        ];
        let pool = sqlx::SqlitePool::new(&url).await?;
        Ok(Self { pool, tables })
    }
    
    pub async fn init_db(&self) -> std::process::Output {
        //does not work
        std::process::Command::new("sh")
            .current_dir("/home/chrisp/scr/rust/memuri")
            .arg("./scripts/init.sh")
            .output()
            .expect("The database successfully initialized")
    }
    
    pub async fn clear_tables(&self) -> sqlx::Result<()> {
        for table in self.tables.iter() {
            self.drop_table(table).await?;
            println!("{} dropped.", table);
        }
        Ok(())
    }

    pub async fn get_users(&self) -> sqlx::Result<Vec<User>> {
        let users = sqlx::query_as::<_, User>
            ("SELECT * FROM Users")
            .fetch_all(&self.pool)
            .await?;

        Ok(users)
    }

    pub async fn add_user(&self, 
        email: String, 
        username: String, 
        password: String)
    -> sqlx::Result<()> {
        sqlx::query(r#"INSERT INTO Users 
                    (email, username, password, date_created)
                    VALUES ($1, $2, $3, $4);"#)
            .bind(email)
            .bind(username)
            .bind(Hasher::new().with_password(password).hash().unwrap())
            .bind(Utc::now().timestamp())
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    pub async fn add_bg(&self, uid: i32, bg: i32) -> sqlx::Result<()> {
        sqlx::query(r#"INSERT INTO BgRecords (uid, bg, date_created)
                    VALUES ($1, $2, $3);"#)
            .bind(uid)
            .bind(bg)
            .bind(Utc::now().timestamp())
            .execute(&self.pool)
            .await?;
        Ok(())
    }

    pub async fn add_rule(
        &self, 
        uid: i32, 
        rule: &str,
        label: Option<&str>,
        priority: Option<i32>,)
    -> sqlx::Result<()> {
        let lbl = label.unwrap_or("");
        let pri = priority.unwrap_or(0);
        sqlx::query(r#"INSERT INTO Rules 
            (uid, rule, label, priority, date_created)
            VALUES ($1, $2, $3, $4, $5);"#)
            .bind(uid)
            .bind(rule.to_string())
            .bind(lbl)
            .bind(pri)
            .bind(Utc::now().timestamp())
            .execute(&self.pool)
            .await?;
        Ok(())
    }

    pub async fn get_rule_id_from_rule(&self, rule: String) 
    -> sqlx::Result<i32> {
        let rid: (i32,) = sqlx::query_as("SELECT rid FROM Rules WHERE rule = $1;")
            .bind(rule)
            .fetch_one(&self.pool)
            .await?;
        Ok(rid.0)
    }

    pub async fn add_rule_log_entry(&self, 
        rid: i32,
        status: Option<i32>,
        label: Option<String>,
        remarks: Option<String>,)
    -> sqlx::Result<()> {
        sqlx::query(r#"INSERT INTO RuleLogs
            (rid, status, label, priority, date_created)
            VALUES ($1, $2, $3, $4, $5);"#)
            .bind(rid)
            .bind(status.unwrap_or(0))
            .bind(label.unwrap_or(String::from("")))
            .bind(remarks.unwrap_or(String::from("")))
            .bind(Utc::now().timestamp())
            .execute(&self.pool)
            .await?;
        Ok(())
    }


    pub async fn add_task(&self, 
        uid: i32, 
        task: String,
        remarks: Option<String>,
        label: Option<String>,
        status: Option<i32>,
        priority: Option<i32>,
        date_due: Option<i32>)
    -> sqlx::Result<()> {
        sqlx::query(r#"INSERT INTO Tasks 
            (uid, task, label, status, 
            priority, date_due, date_created)
            VALUES ($1, $2, $3, $4, $5, $6, $7);"#)
            .bind(uid)
            .bind(task)
            .bind(remarks.unwrap_or(String::from("")))
            .bind(label.unwrap_or(String::from("")))
            .bind(status.unwrap_or(0))
            .bind(priority.unwrap_or(0))
            .bind(date_due.unwrap_or(Utc::now().timestamp() as i32))
            .bind(Utc::now().timestamp())
            .execute(&self.pool)
            .await?;
        Ok(())
    }

    pub async fn del_user(&self, username: &str) -> sqlx::Result<()> {
        sqlx::query(r#"DELETE FROM Users WHERE username=$1"#)
            .bind(username)
            .execute(&self.pool)
            .await?;
        Ok(())
    }

    pub async fn drop_table(&self, table: &str) -> Result<(), sqlx::Error>{
        sqlx::query(r#"DELETE FROM $1;"#)
            .bind(table.to_lowercase())
            .execute(&self.pool)
            .await?;
        Ok(())
    }


    pub async fn create_entry(&self, uid: i32, content: String) {

    }

    
    pub async fn get_user_by_id(&self, id: i32) {

    }

    pub async fn get_user_by_email(&self, email: &str) {

    }

    pub async fn get_user_by_username(&self, username: &str) -> Result<i32, sqlx::Error> {
        let uid: u64 = sqlx::query(r#"SELECT * FROM Users WHERE username = $1;"#)
            .bind(username)
            .execute(&self.pool)
            .await?;
        Ok(uid as i32)
    }
}




// TODO: 
//  * Create "Records" base table which references FoodRecords, BGRecords, etc.?
//
