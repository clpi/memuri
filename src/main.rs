use memuri::db::database::Database;
use memuri::api::{AppState, routes};
use actix_web::{dev::ServiceRequest, middleware, web, App, Error, HttpServer};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use actix_web_httpauth::middleware::HttpAuthentication;
use dotenv::var;
use listenfd::ListenFd;
use actix_web_httpauth::middleware::AuthenticationMiddleware;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {

    let db = Database::new(&var("DATABASE_URL").unwrap())
        .await
        .unwrap();
    let host = var("DEV_HOST").expect("HOST not set");
    let port = var("DEV_PORT").expect("PORT not set");
    let mut listenfd = ListenFd::from_env();

    let mut server = HttpServer::new(move || {
        let data = AppState {
            name: String::from("memuri"),
            db: db.clone(),
        };
        let auth = HttpAuthentication::bearer(memuri::api::validate);
        App::new()
            .data(data)
            .wrap(middleware::Logger::default())
            //.wrap(auth)
            .configure(routes::init)
    });

    server = if let Some(l) = listenfd.take_tcp_listener(0)? {
        server.listen(l)?
    } else {
        server.bind(format!("{}:{}", host, port))?
    };
    server.workers(1)
        .run()
        .await
}
