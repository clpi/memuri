if [ "$1" == "-d" ]; then
    cargo run memuri 2>/dev/null & xdg-open http://127.0.0.1:8888
elif [ "$1" == "-h" ] ; then
    systemfd --no-pid -s http::8989 -- cargo watch -x run
else
    cargo run memuri
fi 
