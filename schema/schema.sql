CREATE TABLE IF NOT EXISTS Users (
    uid INTEGER PRIMARY KEY AUTOINCREMENT,
    email TEXT UNIQUE NOT NULL,
    username TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL,
    date_created INTEGER NOT NULL DEFAULT (STRFTIME ('%s', 'now'))
);

CREATE TABLE IF NOT EXISTS Entries (
    eid INTEGER PRIMARY KEY AUTOINCREMENT,
    uid INTEGER NOT NULL,
    content TEXT,
    label TEXT,
    date_created INTEGER NOT NULL DEFAULT (STRFTIME ('%s', 'now')),
    FOREIGN KEY (uid) REFERENCES Users(uid)
);

CREATE TABLE IF NOT EXISTS Tasks (
    tid INTEGER PRIMARY KEY AUTOINCREMENT,
    uid INTEGER NOT NULL,
    task TEXT,
    remarks TEXT,
    label TEXT,
    status INTEGER NOT NULL DEFAULT 0,
    priority INTEGER NOT NULL DEFAULT 0,
    date_due INTEGER,
    date_created INTEGER NOT NULL DEFAULT (STRFTIME ('%s', 'now')),
    FOREIGN KEY (uid) REFERENCES Users(uid)
);

CREATE TABLE IF NOT EXISTS Rules (
    rid INTEGER PRIMARY KEY AUTOINCREMENT,
    uid INTEGER NOT NULL,
    rule TEXT,
    remarks TEXT,
    label TEXT,
    priority INTEGER NOT NULL DEFAULT 0,
    date_created  INTEGER NOT NULL DEFAULT (STRFTIME ('%s', 'now')),
    FOREIGN KEY (uid) REFERENCES Users(uid)
);

CREATE TABLE IF NOT EXISTS RuleLogs (
    lid INTEGER PRIMARY KEY AUTOINCREMENT,
    rid INTEGER NOT NULL,
    status INTEGER NOT NULL,
    label TEXT,
    remarks TEXT,
    date_created INTEGER NOT NULL DEFAULT (STRFTIME ('%s', 'now')),
    FOREIGN KEY(rid) REFERENCES Rules(rid)
);

CREATE TABLE IF NOT EXISTS BgRecords (
    bid INTEGER PRIMARY KEY AUTOINCREMENT,
    uid INTEGER NOT NULL,
    bg INTEGER NOT NULL,
    date_created TEXT NOT NULL DEFAULT (STRFTIME ('%s', 'now')),
    FOREIGN KEY (uid) REFERENCES Users(uid)
);

CREATE TABLE IF NOT EXISTS FoodRecords (
    fid INTEGER PRIMARY KEY AUTOINCREMENT,
    uid INTEGER NOT NULL,
    food TEXT NOT NULL,
    calories INTEGER NOT NULL,
    carbs INTEGER DEFAULT 0,
    protein INTEGER DEFAULT 0,
    fat INTEGER DEFAULT 0,
    time_eaten INTEGER NOT NULL DEFAULT (STRFTIME ('%s', 'now')),
    date_created INTEGER NOT NULL DEFAULT (STRFTIME ('%s', 'now')),
    FOREIGN KEY (uid) REFERENCES Users(uid)
);

CREATE TABLE IF NOT EXISTS GenericRecords (
    gid INTEGER PRIMARY KEY AUTOINCREMENT,
    uid INTEGER NOT NULL,
    record_key BLOB NOT NULL,
    record_value BLOB NOT NULL,
    label TEXT,
    date_created INTEGER NOT NULL,
    FOREIGN KEY (uid) REFERENCES Users(uid)
);
